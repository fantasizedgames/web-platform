/*
 * Fantasy TD
 * ROUTER.JS
*/

define([
  'views/frontpage/frontpageView',
  'views/game/gameLobbyView',
  'views/game/createGameView',
  'views/game/preGameLobbyView',
  'views/game/gameSummaryView',
  'views/header/headerView',
  'views/footer/footerView',
  'views/information/aboutView',
  'views/information/termsView',
  'views/information/privacyView',
  'views/information/helpView',

], function( FrontpageView, GameLobbyView, CreateGameView, PreGameLobby,
             GameSummaryView, HeaderView, FooterView, AboutView,
             TermsView, PrivacyView, HelpView )
{
  AppRouter = Backbone.Router.extend(
  {
    routes:
    {
      // Define the URL routes.
      '' : 'frontpage',
      'game/lobby' : 'gameLobby',
      'game/create' : 'createGame',
      'game/pregame' : 'preGameLobby',
      'game/summary/:game' : 'gameSummary',
      'about' : 'about',
      'terms' : 'terms',
      'privacy' : 'privacy',
      'help' : 'help',
      // 404.
      '*actions'       : 'defaultAction'
    },

    initialize : function()
    {
      // Load the constant header and footer views.
      var headerView = new HeaderView();
      var footerView = new FooterView();
      // Extend view, with a friendly goto method.
      var self = this;
      Backbone.View.prototype.goto = function(route)
      {
        self.navigate(route, true);
      }
      // Create actual trickers
      this.on('route:about', function()
      {
        var aboutView = new AboutView();
      });
      this.on('route:terms', function()
      {
        var termsView = new TermsView();
      });
      this.on('route:privacy', function()
      {
        var privacyView = new PrivacyView();
      });
      this.on('route:help', function()
      {
        var helpView = new HelpView();
      });
      this.on('route:gameLobby', function()
      {
        var gameLobbyView = new GameLobbyView();
      });
      this.on('route:createGame', function()
      {
        var createGameView = new CreateGameView();
      });
      this.on('route:preGameLobby', function()
      {
        var preGameLobby = new PreGameLobby();
      });
      this.on('route:gameSummary', function( game )
      {
        var gameSummaryView = new GameSummaryView( { gameId : game } );
      });
      this.on('route:frontpage', function ()
      {
        var user = $.cookie('guest');
        // If user is already logged, don't show frontpage.
        if( user ) this.navigate('game/lobby', true);
        // Load fronpageView
        var frontpageView = new FrontpageView();
      });
      Backbone.history.start({pushState : true, root : '/'});
    }
  });
  return AppRouter;
});
