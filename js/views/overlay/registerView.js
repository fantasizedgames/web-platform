define([

  'text!templates/overlay/register.html'

], function( register ) {

  var RegisterView = Backbone.View.extend({
    el: $(".overlay .content"),

    initialize:function () {
      this.render();
    },

    events: {
      'click .close-overview' : 'closeOverview'
    },

    render: function(){
      
      var compiledTemplate = _.template( register );
      this.$el.html( compiledTemplate ); 

      toggleOverlay();
    },

    closeOverview: function() {
      toggleOverlay();
    }
 
  });

  return RegisterView;
  
});
