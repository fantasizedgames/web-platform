define([

  'text!templates/overlay/login.html'

], function( login ) {

  var LoginView = Backbone.View.extend({

    el: $(".overlay .content"),

    initialize:function () {
      this.render();
    },

    events: {
      'click .close-overlay' : 'closeOverview'
    },

    render: function(){
      
      var compiledTemplate = _.template( login );
      this.$el.html( compiledTemplate ); 

      // Toggle our awesome overlay!
      toggleOverlay();
    },

    closeOverview: function() {
      toggleOverlay();
    }

  });

  return LoginView;
  
});
