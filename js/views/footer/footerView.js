define([

  'text!templates/footer/footer.html'

], function( footer ) 
{
  var FooterView = Backbone.View.extend(
  {
    el: $("#footer"),

    initialize: function() 
    {
      this.render();
    },

    render: function() 
    {
      var compiledTemplate = _.template( footer );
      this.$el.html( compiledTemplate ); 
    },

    events: 
    {
      'click .about' : 'goToAbout',
      'click .terms' : 'goToTerms',
      'click .privacy' : 'goToPrivacy',
      'click .help' : 'goToHelp'
    },
    
    goToAbout: function( ev ) 
    {
      var self = this;
      self.goto('about');
    },

    goToTerms: function( ev ) 
    {
      var self = this;
      self.goto('terms');
    },

    goToPrivacy: function( ev ) 
    {
      var self = this;
      self.goto('privacy');
    },

    goToHelp: function( ev ) 
    {
      var self = this;
      self.goto('help');
    }
  });
  return FooterView;
});