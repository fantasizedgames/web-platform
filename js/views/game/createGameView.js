define([
  'text!templates/game/createGame.html',
  'models/game',
  'collections/maps'
], function( createGame, GameModel, MapsCollection ) 
{
  var CreateGameView = Backbone.View.extend(
  {
    el: $("#content"),
    game : new GameModel(),

    initialize : function ( options )
    {
      this.render();
    },

    render : function() 
    {
      var compiledTemplate = _.template( createGame );
      $(this.el).html( compiledTemplate );
    },

    events: 
    {
      'click a[href="next"]' : 'nextStep'
    },

    current : 0,

    nextStep : function( e )
    {
      e.preventDefault();

      var steps = $( '.multistep .step' );

      $(steps[this.current]).bind( 'oanimationend animationend webkitAnimationEnd', function() 
      {
        if( $(this).css('display') == 'block' ) $(this).hide();
      });

      $(steps[this.current]).removeClass('flipInY');
      $(steps[this.current]).addClass('fadeOutLeft');
      this.handleFormState();
      for( var i=0; i < steps.length; i++ ) 
      {
        if( i > this.current ) 
        {
          setTimeout(function () 
          {
            $(steps[i]).show();
            $(steps[i]).addClass('flipInY');
          }, 1000);
          break;
        }
      }
      this.current++;
    },

    handleFormState : function()
    {
      var self = this;
      // Set previous state as completed.
      var label = $( '.stepinfo .step' )[this.current];
      $(label).addClass( 'completed' );

      // Update the map list,
      if( this.current == 1 )
      {
        this.setAvailableMaps();
      }

      // If next step is the last, change the submit button.
      if( this.current >= $( '.multistep .step' ).length - 2 )
      {
        var button = $( 'a[href="next"]' );
        // Alter next button, to be a create button.
        button.removeClass('green').addClass('orange');
        button.html( 'Create' );
      }

      // If this step is the last, call create game.
      if( this.current >= $( '.multistep .step' ).length - 1 )
      {
        this.createGame();
      }
    },

    setAvailableMaps : function()
    {
      var maps = new MapsCollection();
      var self = this;
      // Toggle load, while ajax call is ongoing.
      toggleLoad('.size2of3 .inner > div');
      maps.fetch( {
        data : {playerLimit : $( 'select[name="playerLimit"]' ).val()},
        success : function( maps )
        {
          _.each( maps.models, function( map )
          {
            // Add options to map list, for each fetched map.
            $('<option/>', {
              'text' : map.get('title'),
              'value' : map.get('id')
            }).appendTo( '.multistep .step:nth-child(3) select[name="map"]' );
          });
          toggleLoad( '.size2of3 .inner > div' );
        }
      });
    },

    createGame : function()
    {
      var self = this;
      // Fetch and set each value, to the game, which is about to be created.
      var fields = $( '.multistep .step' ).children( 'input, select' );
      fields.each(function(i, field)
      {
        self.game.set( field.getAttribute( 'name' ), field.value );
      });

      this.game.save( {accesstoken : $.cookie( 'guest' ).accesstoken}, {
        success : function( model, response )
        {
          self.goto('game/pregame');
        }
      });
    }
  });
  return CreateGameView;
});
