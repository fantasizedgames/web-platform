define([
  'text!templates/game/gameLobby.html',
  'collections/games',
  'models/game'
], function( gameLobby, GameCollection, GameModel )
{

  var GameLobbyView = Backbone.View.extend(
  {
    el: $( "#content" ),

    initialize : function ( options )
    {
      this.render();
    },

    render : function()
    {
      var self = this;
      // Start by fetching all active games.
      var games = new GameCollection();
      games.fetch( {
        data : {accesstoken : $.cookie( 'guest' ).accesstoken},
        success : function( games )
        {
          var compiledTemplate = _.template( gameLobby, { games : games } );
          self.$el.html( compiledTemplate );
          $( 'select' ).chosen();
        }
      });
    },

    events: 
    {
      'click .list-item' : 'selectGame',
      'click a' : 'navigate'
    },

    selectGame : function( e )
    {
      // Fetch game id of the selected game.
      var gameId = $( e.currentTarget ).data( 'gameid' );
      var idContainer = $( 'input[name="gameId"]' );
      var joinLink = $( 'a[href="join"]' );

      $( '.selected' ).removeClass( 'selected' );
      // Check if the is being deselected, by being pressed again.
      if( idContainer.attr( 'value' ) == gameId )
      {
        joinLink.addClass( 'blank' );
        idContainer.attr( 'value', '' );
        return;
      }

      // Save the id, and enable the join button.
      idContainer.attr( 'value', gameId );
      if(gameId)
      {
        joinLink.removeClass( 'blank' );
        $( e.currentTarget ).addClass( 'selected' );
      }
    },

    navigate : function( ev )
    {
      ev.preventDefault();

      if( $( ev.target ).attr( 'href' ) == 'create' )
      {
        this.goto( 'game/create' );
      }
      if( $( ev.target ).attr( 'href' ) == 'join'
          && !$( ev.target ).hasClass( 'blank' ) )
      {
        var self = this;
        var game = new GameModel( { id : $('input[name="gameId"]').val() } );
        game.fetch( {
          data : {
            accesstoken : $.cookie( 'guest' ).accesstoken
          },
          success : function()
          {
            game.save({
                accesstoken : $.cookie( 'guest' ).accesstoken,
                update : 'join',
            }, {
              patch : true,
              success : function( model )
              {
                self.goto( 'game/pregame' );
              }
            });
          }
        });
      }
      if( $( ev.target ).attr( 'href' ) == 'refresh' )
      {
        this.render();
      }
    }
  });
  return GameLobbyView;
});
