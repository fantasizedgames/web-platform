define([
  'text!templates/game/gameSummary.html',
  'models/game'
], function( summary, GameModel )
{
  var gameSummaryView = Backbone.View.extend(
  {
    el: $( "#content" ),
    game : new GameModel(),

    initialize : function ( options )
    {
      this.game.set( { id : options.gameId } );
      this.render();
    },

    render : function()
    {
      var self = this;
      // Start by fetching all active games.

      this.game.fetch( {
        data : {accesstoken : $.cookie( 'guest' ).accesstoken},
        success : function( game )
        {
          var compiledTemplate = _.template( summary, { game : game } );
          self.$el.html( compiledTemplate );
        }
      });
    },

    events: 
    {
    }
  });
  return gameSummaryView;
});
