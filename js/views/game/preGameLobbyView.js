define([
  'text!templates/game/preGameLobby.html',
  'models/game'
], function( preGameLobby, GameModel )
{
  var PreGameLobbyView = Backbone.View.extend(
  {
    el: $( "#content" ),
    game : new GameModel( {id : 'undefined'} ),
    countdown : 60,

    initialize : function ( options ) 
    {
      var self = this;
      this.render();
      // Re-render every 5 seconds.
      setInterval(function()
      {
        self.render();
      }, 5000);
    },

    render : function() 
    {
      var self = this;
      this.game.fetch( {
        data : {
          accesstoken : $.cookie('guest').accesstoken
        },
        success : function ( model )
        {
          // If all players is in, start countdown.
          if( self.game.get( 'players' ).length == self.game.get( 'playerLimit' ) )
          {
            self.startTime( false );
          }
          var players = self.game.fetchPlayers();
          // If all players are ready, lower countdown.
          var readyCount = 0;
          _.each( self.game.get( 'playerState' ), function( state )
          {
            if( state.ready )
            {
              readyCount++;
            }
          });

          if( readyCount == self.game.get( 'playerLimit' ) )
          {
            self.startTime( true );
          }
          var compiledTemplate = _.template( preGameLobby, {players : players, game : model} );
          self.$el.html( compiledTemplate );
          self.game.set( 'id', model.get( 'id' ) );
        }
      });
    },

    events: {
      'click a[href="ready"]' : 'ready'
    },

    ready : function( e )
    {
      e.preventDefault();

      var current = $.cookie( 'guest' );
      // Traverse the players, and the set the correct player as ready.
      _.each( this.game.get( 'players' ), function( player )
      {
        if( player.id == current.id )
        {
          player.ready = true;
        }
      });

      this.game.save( {
        accesstoken : $.cookie( 'guest' ).accesstoken,
        update : 'ready'
      }, {patch: true } );
    },

    startTime : function( allReady )
    {
      var self = this;

      // If all is ready, set countdown to 10.
      if( allReady && this.countdown >= 10 )
      {
        this.countdown = 10;
      }
      // Start timer.
      if( this.countdown == 60 )
      {
        setInterval( function ()
        {
          self.countdown--;

          $( '.countdown .inner' ).html( self.countdown );
          if( self.countdown <= 0 )
          {
            window.location = "/ingame?gid=" + self.game.get( 'id' );
          }
        }, 1000 );
      }
    },
  });
  return PreGameLobbyView;
});
