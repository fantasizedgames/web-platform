define([

  'text!templates/header/header.html'

], function( header ) 
{
  var HeaderView = Backbone.View.extend(
  {
    el: $("#header"),

    initialize:function () 
    {
      this.render();
    },

    render: function()
    {
      var compiledTemplate = _.template( header );
      this.$el.html( compiledTemplate ); 
    }
  });
  return HeaderView;
});