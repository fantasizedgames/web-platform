define([

  'text!templates/frontpage/frontpage.html',
  'views/overlay/registerView',
  'views/overlay/loginView',
  'models/guest'

], function( frontpage, RegisterView, LoginView, Guest )
{
  var FrontpageView = Backbone.View.extend(
  {
    el: $("#content"),

    initialize:function ( options )
    {
      this.render();
    },

    render: function()
    {
      var compiledTemplate = _.template( frontpage );
      this.$el.html( compiledTemplate );
    },

    events:
    {
      'submit #guestLogin' : 'createGuest',
      'click a[href="gameLobby"]' : 'createGuest'
    },

    createGuest: function( ev )
    {
      ev.preventDefault();
      // Get nickname.
      var guest = new Guest();

      guest.bind( 'error', function( model, errors )
      {
		    _.each( errors, function( err ) {
          $('input[name=' + err.name + ']').addClass('invalid');
          // add a meesage somewhere, using err.message
			  }, this );
      });

      var self = this;

      guest.save(
      {
        'name' : $('input[name="guestNickname"]').val()
      },{
        success:function( model, response )
        {
          // No need for storing nested deck data.
          delete response.deck['creeps'];
          delete response.deck['towers'];
          // Set cookie with information from server.
          $.cookie('guest', response);
          // Add transition to template.
          self.$el.children('div').addClass('pageout');
          // wait for animation to finish, and change the page.
          setTimeout(function()
          {
            self.goto('game/lobby');
          }, 1000);
        },

        error: function(model, error)
        {
          console.log(model);
          console.log(error);
        }
      });
    },
  });
  return FrontpageView;
});
