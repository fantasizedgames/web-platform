define([

  'text!templates/information/privacy.html',

], function( privacy ) 
{
  var PrivacyView = Backbone.View.extend(
  {
    el: $("#content"),

    initialize:function ( options ) 
    {
      this.render();
    },

    render: function() 
    {
      var compiledTemplate = _.template( privacy );
      this.$el.html( compiledTemplate );
    },
  });
  return PrivacyView;
});
