define([

  'text!templates/information/terms.html',

], function( terms ) 
{
  var TermsView = Backbone.View.extend(
  {
    el: $("#content"),

    initialize:function ( options )
    {
      this.render();
    },

    render: function() 
    {
      var compiledTemplate = _.template( terms );
      this.$el.html( compiledTemplate );
    },
  });
  return TermsView;
});
