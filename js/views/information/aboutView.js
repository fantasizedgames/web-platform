define([

  'text!templates/information/about.html',

], function( about ) 
{
  var AboutView = Backbone.View.extend(
  {
    el: $("#content"),

    initialize:function ( options ) 
    {
      this.render();
    },

    render: function() 
    {
      var compiledTemplate = _.template( about );
      this.$el.html( compiledTemplate );
    },
  });
  return AboutView;
});
