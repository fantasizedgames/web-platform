define([

  'text!templates/information/help.html',

], function( help ) 
{
  var HelpView = Backbone.View.extend(
  {
    el: $("#content"),

    initialize:function ( options ) 
    {
      this.render();
    },

    render: function() 
    {
      var compiledTemplate = _.template( help );
      this.$el.html( compiledTemplate );
    },
  });
  return HelpView;
});
