// js/models/guest.js

define([
], function() {
  Guest = Backbone.Model.extend({

    urlRoot : ftd.settings.rest + '/guest',

    defaults: {
      name : ''
    },

    validate: function( attr ) {
      var errors = [];
      if(attr.name.length < 4) {
        errors.push({message : "You nickname, must be atleast 4 chars", name : 'guestNickname'});
      }

      if(errors.length > 0) {
        return errors;
      }
    }
  });

  return Guest;
});
