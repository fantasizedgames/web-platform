// js/models/game.js

define([
  'models/guest',
  'collections/guests'
], function( GuestModel, GuestCollection )
{
  Game = Backbone.Model.extend({

    urlRoot : ftd.settings.rest + '/game',

    defaults: {
      title: '',
      created : 0,
      completed : 0,
      active: true,
      password : '',
      map : '',
      mode : 'coop',
      difficulty : 'Graceful',
      playerLimit : 0,
      players : []
    },

    validate: function( attr )
    {

    },

    // Have async ajax in mind.
    fetchPlayers : function()
    {
      var self = this;
      var players = [];
      _.each( this.get( 'players' ), function( player )
      {
        if( player.name.match( /^\[Guest\]\s{1}/ )  )
        {
          var model = new GuestModel( { id : player.id } );
          model.fetch({
            data : {
              accesstoken : $.cookie( 'guest' ).accesstoken
            },
            async : false,
            success : function ( player )
            {
              players.push( player );
            }
          });
        }
      });
      // Later fetch users aswell.
      return players;
    }
  });

  return Game;
});
