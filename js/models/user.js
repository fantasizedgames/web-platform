// js/models/user.js

define([
], function() {
  var User = Backbone.Model.extend({

    defaults: {
      username: '',
      email: ''
    }
  });
  return User;
});
