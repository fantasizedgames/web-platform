// js/models/map.js

define([
], function() {
  Map = Backbone.Model.extend({

    urlRoot : ftd.settings.rest + '/map',

    defaults: {
      title : '',
      playerLimit : 0,
      mode : ''
    },

  });

  return Map;
});
