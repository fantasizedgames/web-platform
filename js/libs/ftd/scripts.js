
/**
 * FantasyTD overlay script.
 */

// FantasyTD global application settings.
ftd = {};
ftd.settings = {
  'rest' : '/rest/app_dev.php'
};

ftd.getRealColor = function(colorCode, format)
{
  var text, rgb;
  if(colorCode == 0)
  {
    text = 'red';
    rgb = '255, 96, 96';
  }
  if(colorCode == 1)
  {
    text = 'blue';
    rgb = '81, 179, 255';
  }
  if(colorCode == 2)
  {
    text = 'green';
    rgb = '99, 227, 85';
  }
  if(colorCode == 3)
  {
    text = 'yellow';
    rgb = '240, 238, 84';
  }
  if(colorCode == 4)
  {
    text = 'brown';
    rgb = '190, 154, 90';
  }
  if(colorCode == 5)
  {
    text = 'purple';
    rgb = '212, 124, 222';
  }
  if(colorCode == 6)
  {
    text = 'orange';
    rgb = '255, 172, 53';
  }
  if(colorCode == 7)
  {
    text = 'teal';
    rgb = '127, 245, 245';
  }

  if(format == 'text')
  {
    return text;
  }else {
    return rgb;
  }
}


function toggleOverlay() {
  var overlay = $('.overlay');

  if(overlay.css('display') == 'block') {

    setTimeout( function() {
      overlay.css({'background-color' : 'rgba(0,0,0,0.0)'});
      overlay.children('.content').css({top : 'auto'});
    });    
    overlay.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
      overlay.css({display : 'none'});
      overlay.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");
    });
  }
  else {
    overlay.css({display : 'block'});
    setTimeout( function() {
      overlay.css({'background-color' : 'rgba(0,0,0,0.2)'});
      overlay.children('.content').css({top : '50%'});
    });
  }
}

function toggleLoad( selector )
{
  if( $( selector + ' .load' ).length == 0 )
  {
    $( selector ).css( 'position', 'relative' );
    var load = $( '<div/>', {'class' : 'load'} ).appendTo( selector );
    var icon = $( '<div/>', {'class' : 'icon'} ).appendTo( load );
  } else {
    $( selector + ' .load' ).remove();
  }
}

// Extend underscore with capitalize method.
_.mixin({
  capitalize : function(string) {
    return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
  }
});