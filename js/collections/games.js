// js/collections/games.js

define([
  'models/game'
], function( GameModel)
{

  var GameCollection = Backbone.Collection.extend({
    model: GameModel,
    url : ftd.settings.rest + '/game'
  });

  return GameCollection;
});
