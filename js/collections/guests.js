// js/collections/guests.js

define([
  'models/guest'
], function( GuestModel )
{

  var GuestCollection = Backbone.Collection.extend({
    model: GuestModel,
    url : ftd.settings.rest + '/guest'
  });

  return GuestCollection;
});
