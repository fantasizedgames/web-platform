// js/collections/maps.js

define([
  'models/map'
], function(MapModel)
{

  var MapsCollection = Backbone.Collection.extend({
    model : MapModel,
    url : ftd.settings.rest + '/map'

  });

  return MapsCollection;
});

