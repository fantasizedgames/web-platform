/*
 * Fantasy TD
 * MAIN.JS
*/

require.config(
{
  paths: 
  {
    // Templates directory
    templates: '../templates/',
    // Libraries needed
    text: 'libs/require/text'
  },
  // Append query string, to avoid browser caching, while in dev.
  urlArgs: "bust=" + (new Date()).getTime(),
});

require([
  // Load our app module
  'router',
], function( Router ) 
{
  // Enable json in cookies.
  $.cookie.json = true;
  // Start the Single Paged Application known as Fantasy TD
  var router = new Router();
});
